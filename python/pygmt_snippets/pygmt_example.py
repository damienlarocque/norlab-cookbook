import pygmt
import numpy as np

minlon, maxlon = -71.45, -71.03
minlat, maxlat = 46.71, 46.92
topo_data = '@earth_relief_03s'
fig = pygmt.Figure()
pygmt.makecpt(
    cmap='topo',
    series='-8000/8000/1000',
    continuous=True
)
fig.grdimage(
    grid=topo_data,
    region=[minlon, maxlon, minlat, maxlat],
    projection='M4i'
)
fig.grdimage(
    grid=topo_data,
    region=[minlon, maxlon, minlat, maxlat],
    projection='M4i',
    shading=True,
    frame=True
)
fig.coast(
    region=[minlon, maxlon, minlat, maxlat],
    projection='M4i',
    shorelines=True,
    frame=True
)
fig.grdcontour(
    grid=topo_data,
    interval=4000,
    annotation="4000+f6p",
    limit="-8000/0",
    pen="a0.15p"
)
## Generate fake coordinates in the range for plotting
# lons = minlon + np.random.rand(10)*(maxlon-minlon)
# lats = minlat + np.random.rand(10)*(maxlat-minlat)
# plot data points
# fig.plot(
#    x=lons,
#    y=lats,
#    style='c0.1i',
#    color='red',
#    pen='black',
#    label='something',
#)
# Plot colorbar
fig.colorbar(
    frame='+l"Topography"'
)
# For vertical colorbar
fig.colorbar(
frame='+l"Topography"',
position="x11.5c/6.6c+w6c+jTC+v"
)
# save figure
fig.show() #fig.show(method='external')
# save figure
fig.savefig("topo-plot.png", crop=True, dpi=300, transparent=True)



    

